﻿using CalidadT2.Controllers;
using CalidadT2.Models;
using Final_Calidad.Respositories;
using Final_Calidad.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace CalidadT2Test.ControllerTest
{
    class AuthControllerTest
    {
        [Test]
        public void Test_1_Login_Correcto()
        {
            var mockRepository = new Mock<IAuthRepository>();
            var cookie = new Mock<ICookieAuthService>();
            mockRepository.Setup(o => o.FindUser("pabloa", It.IsAny<String>()))
                .Returns(new Usuario { Username = "pabloa", Password = "12345" });


            var controller = new AuthController(mockRepository.Object, cookie.Object);

            var view = controller.Login("pabloa", "12345");

            Assert.IsInstanceOf<RedirectToActionResult>(view);
        }

        [Test]
        public void Test_2_Login_Erroneo()
        {
            var mockRepository = new Mock<IAuthRepository>();
            var cookie = new Mock<ICookieAuthService>();
            mockRepository.Setup(o => o.FindUser("admin", It.IsAny<String>()));


            var controller = new AuthController(mockRepository.Object, cookie.Object);

            var view = controller.Login("admin", "123");

            Assert.IsInstanceOf<ViewResult>(view);
        }
    }
}
