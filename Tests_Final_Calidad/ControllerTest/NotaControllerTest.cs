﻿using CalidadT2.Controllers;
using CalidadT2.Models;
using CalidadT2.Respositories;
using DiarsFinal.Controllers;
using Final_Calidad.Models;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace CalidadT2Test.ControllerTest
{
    class NotaControllerTest
    {


        [Test]
        public void Test_1_ObtenerListaNotas_Correctamente()
        {
            var repositoryB = new Mock<INotaRepository>();
            var controller = new NotaController(repositoryB.Object);
            var view = controller.Index();
            Assert.IsInstanceOf<ViewResult>(view);
        }

        [Test]
        public void Test_2_ObtenerDetalle()
        {
            var repositoryB = new Mock<INotaRepository>();
            var controller = new NotaController(repositoryB.Object);
            var view = controller.VerDetalle(4);
            Assert.IsInstanceOf<ViewResult>(view);
        }

        [Test]
        public void Test_3_CrearNuevaNota()
        {
            var repositoryB = new Mock<INotaRepository>();
            var controller = new NotaController(repositoryB.Object);

            List<int> cat = new List<int>() {1,2,3};

            var view = controller.Crear(new Nota() { Titulo = "Prueba Unitaria", Contenido = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" }, 
                                        cat);
            Assert.IsInstanceOf<RedirectToActionResult>(view);
        }


        [Test]
        public void Test_4_Editar()
        {
            var repositoryB = new Mock<INotaRepository>();
            var controller = new NotaController(repositoryB.Object);
            var view = controller.Editar(new Nota() {Id=28, Titulo = "Prueba Unitaria", Contenido = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" });
            Assert.IsInstanceOf<RedirectToActionResult>(view);
        }

        [Test]
        public void Test_5_EliminarNota()
        {
            var repositoryB = new Mock<INotaRepository>();
            var controller = new NotaController(repositoryB.Object);
            var view = controller.Eliminar(1);
            Assert.IsInstanceOf<RedirectToActionResult>(view);
        }
    }
}
