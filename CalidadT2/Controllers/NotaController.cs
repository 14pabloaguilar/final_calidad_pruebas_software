﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CalidadT2.Models;
using CalidadT2.Respositories;
using Final_Calidad.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;

namespace DiarsFinal.Controllers
{
    public class NotaController : Controller
    {
        private readonly INotaRepository repositoryN;

        public NotaController(INotaRepository repositoryN)
        {
            this.repositoryN = repositoryN;
        }


        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult _IndexB()
        {
            var query=repositoryN.getNotas();
            ViewBag.posts = query;
            return View();
        }

        public IActionResult VerDetalle(int id)
        {
            ViewBag.tag = repositoryN.DetalleTags(id);
            ViewBag.posts = repositoryN.DetallePost(id);
            ViewBag.tagsDelPost = repositoryN.DetalleTagsPost();
            ViewBag.detalle = repositoryN.DetalleNotaCat(id);
            return View();
        }

        [HttpGet]
        public IActionResult Crear()
        {
            var categorias = repositoryN.DetalleTagsPost();
            ViewBag.cat = categorias;
            return View(new Nota());
        }

        [HttpPost]
        public IActionResult Crear(Nota nota, List<int> tags)
        {
            var categorias = repositoryN.DetalleTagsPost();
            ViewBag.cat = categorias;
            if (ModelState.IsValid)
            {
                repositoryN.Crear(nota, tags);
            }
                return RedirectToAction("Index");
        }

        
        public IActionResult Eliminar(int ID)
        {
            repositoryN.Eliminar(ID);
            return RedirectToAction("Index","Post");
        }

       
        [HttpGet]
        public IActionResult Editar(int id)
        {

            var post = repositoryN.Editar(id);

            ViewBag.Id = post.Id;
            ViewBag.Post = post;
            ViewBag.Contenido = post.Contenido;

            return View(post);
        }


        [HttpPost]
        public IActionResult Editar( Nota post )
        {

            repositoryN.Editar2(post);

            return RedirectToAction( "Index" );
        }


        public IActionResult Tags(int Id)
        {

            var tags = repositoryN.DetalleTagsPost();
            var post = repositoryN.getNotas();

            ViewBag.Detalles = repositoryN.DetalleNotaCat(Id);
            ViewBag.Tags = tags;
            ViewBag.Post = post;
            return View();
        }

    }
}