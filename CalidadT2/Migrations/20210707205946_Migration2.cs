﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Final_Calidad.Migrations
{
    public partial class Migration2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Nota_Usuario_UsuarioId",
                table: "Nota");

            migrationBuilder.DropIndex(
                name: "IX_Nota_UsuarioId",
                table: "Nota");

            migrationBuilder.DropColumn(
                name: "UsuarioId",
                table: "Nota");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UsuarioId",
                table: "Nota",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Nota_UsuarioId",
                table: "Nota",
                column: "UsuarioId");

            migrationBuilder.AddForeignKey(
                name: "FK_Nota_Usuario_UsuarioId",
                table: "Nota",
                column: "UsuarioId",
                principalTable: "Usuario",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
