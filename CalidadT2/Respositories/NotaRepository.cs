﻿using CalidadT2.Models;
using Final_Calidad.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CalidadT2.Respositories
{
    public interface INotaRepository{
        public List<Nota> getNotas();
        public List<Nota> DetallePost(int id);
        public List<Categoria> DetalleTags(int id);
        public List<Categoria> DetalleTagsPost();
        public List<DetalleNotaCategoria> DetalleNotaCat(int id);
        public void Crear(Nota nota, List<int> tags);
        public void Eliminar(int ID);
        public Nota Editar(int id);
        public void Editar2(Nota post);
    }
    public class NotaRepository : INotaRepository
    {

        public AppFCContext _context;

        public NotaRepository(AppFCContext context)
        {
            _context = context;
        }

        public List<DetalleNotaCategoria> DetalleNotaCat(int id)
        {
            return _context.DetalleNotaCategorias.ToList();
        }

        public List<Nota> DetallePost(int id)
        {
            return _context.Notas.Where(a => a.Id == id).ToList();
        }

        public List<Categoria> DetalleTags(int id)
        {
            return _context.Categorias.Where(a => a.Id == id).ToList();
        }

        public List<Categoria> DetalleTagsPost()
        {
            return _context.Categorias.ToList();
        }

        public List<Nota> getNotas()
        {
            return _context.Notas.ToList();
        }
        public void Crear(Nota nota, List<int> tags)
        {

                var fechita = DateTime.Now;
                nota.Fecha = fechita;
                _context.Add(nota);
                _context.SaveChanges();
                //Agregar tags
                var postss = _context.Notas.First(o => o.Titulo == nota.Titulo);

                foreach (var tagc in tags)
                {
                    var detalle = new DetalleNotaCategoria(postss.Id, tagc);
                    _context.Add(detalle);
                    _context.SaveChanges();
                }
        }

        public void Eliminar(int ID)
        {
            var eliminarPost = _context.Notas.First(a => a.Id == ID);
            _context.Notas.Remove(eliminarPost);
            _context.SaveChanges();
        }

        public Nota Editar(int id)
        {
            return _context.Notas.Where(o => o.Id == id).FirstOrDefault(); ;
        }

        public void Editar2(Nota post)
        {
            var posts=  _context.Notas.Where(o => o.Id == post.Id).FirstOrDefault();
            posts.Titulo = post.Titulo;
            posts.Fecha = DateTime.Now;
            posts.Contenido = post.Contenido;
            _context.SaveChanges();
        }
    }
}
