﻿using CalidadT2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Final_Calidad.Respositories
{
    public interface IAuthRepository
    {
        public Usuario GetUserLogin(Claim claim);
        public Usuario FindUser(String user, String password);
    }

    public class AuthRepository : IAuthRepository
    {
        private AppFCContext _context;

        public AuthRepository(AppFCContext context)
        {
            _context = context;
        }

        public Usuario FindUser(string user, string password)
        {
            var Usuario = _context.Usuarios.Where(o => o.Username == user && o.Password == password).FirstOrDefault();
            return Usuario;
        }

        public Usuario GetUserLogin(Claim claim)
        {
            var user = _context.Usuarios.Where(o => o.Username == claim.Value).FirstOrDefault();
            return user;
        }

    }
}
