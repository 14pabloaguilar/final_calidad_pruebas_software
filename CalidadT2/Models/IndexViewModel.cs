﻿using Final_Calidad.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Final_Calidad.Models
{
    public class IndexViewModel:ModeloBase
    {
        public List<Nota> Nota { get; set; }
    }
}
