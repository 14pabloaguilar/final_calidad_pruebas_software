﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Final_Calidad.Models
{
    public class DetalleNotaCategoria
    {
        public int Id { get; set; }
        public int IdNota{ get; set; }
        public int IdCategoria { get; set; }
        public Categoria Categoria { get; set; }
        public Nota Nota { get; set; }

        public DetalleNotaCategoria()
        {
        }
        public DetalleNotaCategoria(int IdNota, int IdCategoria)
        {
            this.IdNota = IdNota;
            this.IdCategoria = IdCategoria;
        }

    }
}
