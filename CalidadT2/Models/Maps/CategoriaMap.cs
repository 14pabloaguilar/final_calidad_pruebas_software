﻿using Final_Calidad.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Final_Calidad.BD.Maps
{
    //Este para varios
    public class CategoriaMap : IEntityTypeConfiguration<Categoria>
    {

        public void Configure(EntityTypeBuilder<Categoria> builder)
        {
            builder.ToTable("Categoria");
            builder.HasKey(x => x.Id);

            builder.HasMany(o => o.DetalleNotaCategoria)
                .WithOne(o => o.Categoria)
                .HasForeignKey(o => o.IdCategoria);
        }
    }
}
