﻿using Final_Calidad.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Final_Calidad.BD.Maps
{
  
        public class DetalleNotaCategoriaMap : IEntityTypeConfiguration<DetalleNotaCategoria>
        {
        //Este es uno solo
        public void Configure(EntityTypeBuilder<DetalleNotaCategoria> builder)
        {

            builder.ToTable("DetalleNotaCategoria");
            builder.HasKey(x => x.Id);

            builder.HasOne(a => a.Categoria).WithMany(a => a.DetalleNotaCategoria).
                HasForeignKey(a => a.IdCategoria);

            builder.HasOne(o => o.Nota).WithMany(o => o.DetalleNotaCategoria)
                .HasForeignKey(o => o.IdNota);
        }
    }
}
