﻿using Final_Calidad.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Final_Calidad.BD.Maps
{
    public class NotaMap : IEntityTypeConfiguration<Nota>
    {
        //Este es uno solo
        public void Configure(EntityTypeBuilder<Nota> builder)
        {

            builder.ToTable("Nota");
            builder.HasKey(x => x.Id);

            builder.HasMany(o => o.DetalleNotaCategoria)
                .WithOne(o => o.Nota)
                .HasForeignKey(o => o.IdNota);

        }
    }
}
