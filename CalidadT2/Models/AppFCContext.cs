﻿using CalidadT2.Models.Maps;
using Final_Calidad.BD.Maps;
using Final_Calidad.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalidadT2.Models
{
    public class AppFCContext: DbContext
    {
        public DbSet<Categoria> Categorias { get; set; }
        public DbSet<Nota> Notas { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<DetalleNotaCategoria> DetalleNotaCategorias { get; set; }

        public AppFCContext(DbContextOptions<AppFCContext> options)
            : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new DetalleNotaCategoriaMap());
            modelBuilder.ApplyConfiguration(new NotaMap());
            modelBuilder.ApplyConfiguration(new UsuarioMap());
            modelBuilder.ApplyConfiguration(new CategoriaMap());
        }
    }
}
